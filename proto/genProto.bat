@echo off
chcp 65001
set CS_OUT_DIR=output_csharp
set JAVA_OUT_DIR=../src/main/java

if not exist %CS_OUT_DIR% mkdir %CS_OUT_DIR%

echo “清理旧的proto文件...”

del /f /s /q %CS_OUT_DIR%\*
del /f /s /q %JAVA_OUT_DIR%\*

echo “开始将proto文件编译为C#与Java...“

protoc-25.0-win64\bin\protoc.exe -I=msg msg/*.proto --csharp_out=%CS_OUT_DIR% --java_out=%JAVA_OUT_DIR%

echo 全部完成.
