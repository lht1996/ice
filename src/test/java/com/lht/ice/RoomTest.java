package com.lht.ice;

import com.lht.ice.data.*;
import com.lht.ice.net.proto.Room;
import org.junit.jupiter.api.Test;

/**
 * @author 乐浩天
 */
public class RoomTest {
    @Test
    public void test() {
        AnchorRoom room = new AnchorRoom(new Anchor());
        room.start(Room.GameType.ordinary, 1);
        LiveComment liveComment = new LiveComment();
        liveComment.setSec_openid("111");
        liveComment.setNickname("111");
        liveComment.setAvatar_url("111");
        liveComment.setContent("2");
        room.comment(liveComment);
        assert room.round().getRoom().getRight().getPulling() == 10;
        liveComment.setContent("666");
        room.comment(liveComment);
        assert room.round().getRoom().getRight().getPulling() == 11;
        room.round();
        room.round();
        room.round();
        assert room.round().getRoom().getRight().getPulling() == 10;
        liveComment.setContent("666");
        room.comment(liveComment);
        room.round();
        LiveLike liveLike = new LiveLike();
        liveLike.setSec_openid("111");
        liveLike.setLike_num(5);
        room.like(liveLike);
        assert room.round().getRoom().getRight().getPulling() == 16;
        room.round();
        room.round();
        assert room.round().getRoom().getRight().getPulling() == 15;
        assert room.round().getRoom().getRight().getPulling() == 10;
        liveComment.setSec_openid("222");
        liveComment.setContent("1");
        room.comment(liveComment);
        assert room.round().getRoom().getLeft().getPulling() == 10;
        liveLike.setSec_openid("222");
        liveLike.setLike_num(100);
        room.like(liveLike);
        room.round();
        room.round();
        LiveGift liveGift = new LiveGift();
        liveGift.setSec_openid("111");
        liveGift.setSec_gift_id("pGLo7HKNk1i4djkicmJXf6iWEyd+pfPBjbsHmd3WcX0lerm2UdnRR7UINvl=");
        liveGift.setGift_num(1);
        room.gift(liveGift);
        room.round();
        int leftPulling = room.getRightSide().getPulling();
        liveGift.setSec_openid("222");
        liveGift.setSec_gift_id("P7zDZzpeO215SpUptB+aURb1+zC14UC9MY1+MHszKoF0p5gzYk8CNEbey60=");
        liveGift.setGift_num(1);
        room.gift(liveGift);
        int leftPulling2 = room.getRightSide().getPulling();
        System.out.println(leftPulling + " " + leftPulling2);
        room.round();
        room.round();
        room.round();
        room.round();
        room.round();
        room.round();
        room.round();
        int leftPulling3 = room.getRightSide().getPulling();
        System.out.println(leftPulling + " " + leftPulling3);
        for (int i = 0; i < 100000; i++) {
            room.round();
        }
    }
}
