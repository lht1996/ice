package com.lht.ice;

import com.google.protobuf.Message;
import com.lht.ice.net.HandlerRegister;
import com.lht.ice.net.proto.Common;
import com.lht.ice.net.proto.Login;
import com.lht.ice.net.proto.Room;
import com.lht.ice.ticker.GetTokenTicker;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import java.io.IOException;

@SpringBootTest
class TugOfWarApplicationTests {
    @MockBean
    private GetTokenTicker tokenTicker;

    StandardWebSocketClient client = new StandardWebSocketClient();
    WebSocketSession session;

    {
        try {
            session = client.execute(new TestWebSocketHandler(), "ws://dev.lht.show/ws").get();
//            session = client.execute(new TestWebSocketHandler(), "ws://127.0.0.1:9367/ws").get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void start() {
        Login.CreateRoomReq createRoomReq = Login.CreateRoomReq.newBuilder().setToken("test").build();
        sendMsg(session, createRoomReq);
        Login.StartBattleReq startBattleReq = Login.StartBattleReq.newBuilder().setGameType(Room.GameType.ordinary).build();
        sendMsg(session, startBattleReq);
        // player1 加入 1
        Login.GmReq gmReq1 = Login.GmReq.newBuilder().setCommand("comment player2 2").build();
        sendMsg(session, gmReq1);
        Login.GmReq gmReq2 = Login.GmReq.newBuilder().setCommand("comment player1 1").build();
        sendMsg(session, gmReq2);
        // player1 666
        Login.GmReq gmReq3 = Login.GmReq.newBuilder().setCommand("comment player1 666").build();
        sendMsg(session, gmReq3);
        // player1 点10赞
        Login.GmReq gmReq4 = Login.GmReq.newBuilder().setCommand("like player1 300").build();
        sendMsg(session, gmReq4);
        // player1 送3礼物 2个
        Login.GmReq gmReq5 = Login.GmReq.newBuilder().setCommand("gift player1 3 1").build();
        sendMsg(session, gmReq5);
        while (true) {

        }
    }

    @Scheduled(cron = "* * * * * *")
    public void heartbeat() {
        Login.HeartbeatReq heartbeat = Login.HeartbeatReq.newBuilder().setTimestamp(System.currentTimeMillis()).build();
        sendMsg(session, heartbeat);

    }

    private static void sendMsg(WebSocketSession session, Message msg) {
        if (!session.isOpen()) {
            return;
        }
        Common.ReqMsg.Builder req = Common.ReqMsg.newBuilder();
        req.setOpCode(msg.getDescriptorForType().getOptions().getExtension(Common.opcode));
        req.setData(msg.toByteString());
        try {
            session.sendMessage(new BinaryMessage(req.build().toByteArray()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static class TestWebSocketHandler extends BinaryWebSocketHandler {

        @Override
        protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
            Common.RespMsg reqMsg = Common.RespMsg.parseFrom(message.getPayload());
            int opCode = reqMsg.getOpCode();
            Message req = HandlerRegister.getMessage(opCode, reqMsg.getData());
            System.out.println(req);
        }

        @Override
        public void afterConnectionEstablished(WebSocketSession session) throws Exception {
            super.afterConnectionEstablished(session);
            System.out.println("连接");
        }

        @Override
        public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
            super.afterConnectionClosed(session, status);
            System.out.println("断开");
        }
    }
}
