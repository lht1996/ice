package com.lht.ice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lht.ice.config.Json;
import com.lht.ice.data.*;
import com.lht.ice.net.proto.Room;
import com.lht.ice.service.RedisKey;
import com.lht.ice.ticker.BattleTicker;
import com.lht.ice.tools.SignUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author 乐浩天
 */
@RestController
@Slf4j
public class ReceivePushController {

    @Autowired
    private BattleTicker battleTicker;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @GetMapping("/test")
    public String test() {
        return "test";
    }

    @PostMapping(value = "/receivePush", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> receiveLiveMessage(@RequestHeader Map<String, String> headers, @RequestBody String body) {
        log.info("headers" + headers + "body" + body);
        if (!SignUtil.signatureCheck(headers, body)) {
            throw new RuntimeException("签名校验失败");
        }
        String roomId = headers.get("x-roomid");
        String msgType = headers.get("x-msg-type");
        AnchorRoom room = battleTicker.getRoomMap().get(Long.parseLong(roomId));
        if (room.getStatus() != Room.RoomStatus.start) {
            return ResponseEntity.ok("ok");
        }
        switch (msgType) {
            case "live_comment" -> {
                List<LiveComment> liveComments = parseJsonToList(body, LiveComment.class);
                for (LiveComment liveComment : liveComments) {
                    String playerId = room.comment(liveComment);
                    if (playerId != null) {
                        String outJson = (String) redisTemplate.opsForHash().get(RedisKey.Player.name(), playerId);
                        PlayerData playerData = null;
                        if (outJson != null) {
                            playerData = Json.jsonToObject(outJson, PlayerData.class);
                        }
                        if (playerData == null) {
                            playerData = new PlayerData();
                            playerData.setOpenId(playerId);
                            playerData.setWinNum(0);
                        }
                        Player player = room.getPlayerMap().get(playerId);
                        player.setWinNum(playerData.getWinNum());
                        player.setBear(playerData.getTotalBear());
                        playerData.setIcon(liveComment.getAvatar_url());
                        playerData.setName(liveComment.getNickname());
                        String json = Json.objectToJson(playerData);
                        redisTemplate.opsForHash().put(RedisKey.Player.name(), playerId, json);
                    }
                }
            }
            case "live_gift" -> {
                List<LiveGift> liveGifts = parseJsonToList(body, LiveGift.class);
                for (LiveGift liveGift : liveGifts) {
                    room.gift(liveGift);
                }
            }
            case "live_like" -> {
                List<LiveLike> liveLikes = parseJsonToList(body, LiveLike.class);
                for (LiveLike liveLike : liveLikes) {
                    room.like(liveLike);
                }
            }
        }
        return ResponseEntity.ok("ok");
    }

    private <T> List<T> parseJsonToList(String json, Class<T> clazz) {
        if (json == null || json.isEmpty() || clazz == null) {
            throw new IllegalArgumentException("JSON string and class cannot be null or empty");
        }

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            List<T> resultList = objectMapper.readValue(json, objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
            return resultList;
        } catch (JsonProcessingException e) {
            // 处理JSON解析异常
            e.printStackTrace(); // 或者抛出自定义异常
            return Collections.emptyList();
        }
    }

}
