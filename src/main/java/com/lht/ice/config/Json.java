package com.lht.ice.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lht.ice.data.MetaGift;
import com.lht.ice.net.proto.Room;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 乐浩天
 */
public class Json {
    public final static ObjectMapper objectMapper = new ObjectMapper();
    public final static Map<String, MetaGift> giftIdToMeta = new HashMap<>();
    public final static Map<Room.ShipType, MetaGift> shipTypeToMeta = new HashMap<>();

    static {
        initMeta("n1/Dg1905sj1FyoBlQBvmbaDZFBNaKuKZH6zxHkv8Lg5x2cRfrKUTb8gzMs=", 0, 0, 5);
        initMeta("28rYzVFNyXEXFC8HI+f/WG+I7a6lfl3OyZZjUS+CVuwCgYZrPrUdytGHu0c=", 1, 364, 90);
        initMeta("fJs8HKQ0xlPRixn8JAUiL2gFRiLD9S6IFCFdvZODSnhyo9YN8q7xUuVVyZI=", 2, 693, 90);
        initMeta("PJ0FFeaDzXUreuUBZH6Hs+b56Jh0tQjrq0bIrrlZmv13GSAL9Q1hf59fjGk=", 3, 1393, 120);
        initMeta("IkkadLfz7O/a5UR45p/OOCCG6ewAWVbsuzR/Z+v1v76CBU+mTG/wPjqdpfg=", 4, 3640, 150);
        initMeta("gx7pmjQfhBaDOG2XkWI2peZ66YFWkCWRjZXpTqb23O/epru+sxWyTV/3Ufs=", 5, 8400, 180);
    }

    private static void initMeta(String id, int type, int pulling, int duration) {
        MetaGift metaGift = new MetaGift(id, type, pulling, duration);
        giftIdToMeta.put(metaGift.getId(), metaGift);
        shipTypeToMeta.put(Room.ShipType.forNumber(metaGift.getType()), metaGift);
    }

    public static String objectToJson(Object object) {
        try {
            // Convert object to JSON string
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <V> V jsonToObject(String json, Class<V> clazz) {
        try {
            return objectMapper.readValue(json, clazz);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(Json.giftIdToMeta.keySet().toArray(new String[0])));
    }

    private static void getString(long timestamp) {
        Instant instant = Instant.ofEpochMilli(timestamp);

        // Convert Instant to LocalDate
        LocalDate localDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
        // Format the year and week values
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyww");
        String formattedDate = formatter.format(localDate);
        System.out.println("Formatted date: " + formattedDate);
    }
}
