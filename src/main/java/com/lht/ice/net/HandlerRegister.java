package com.lht.ice.net;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.Message;
import com.lht.ice.net.proto.Common;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class HandlerRegister {
    private static final Map<Integer, ProtoHandler> handlerMap = new ConcurrentHashMap<>();
    private static final BiMap<Integer, String> codeToProto = HashBiMap.create();
    private static final Map<Integer, Method> codeToParseFrom = new ConcurrentHashMap<>();
    @Autowired
    private ApplicationContext applicationContext;
    private static ApplicationContext SAC;

    public static ApplicationContext getApplicationContext() {
        return SAC;
    }

    @PostConstruct
    public void registerHandlers() {
        SAC = applicationContext;
        String packageName = "com.lht.ice.net.proto";
        Set<BeanDefinition> protoSet = getBeanDefinitions(packageName, GeneratedMessageV3.class);

        for (BeanDefinition proto : protoSet) {
            String className = proto.getBeanClassName();
            // 反射获取类
            try {
                Class<?> protoClass = Class.forName(className);
                Descriptors.Descriptor descriptor = (Descriptors.Descriptor) protoClass.getMethod("getDescriptor").invoke(null);
                Method parseFrom = protoClass.getMethod("parseFrom", ByteString.class);
                Integer opCode = descriptor.getOptions().getExtension(Common.opcode);
                codeToProto.put(opCode, className);
                codeToParseFrom.put(opCode, parseFrom);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        String packageName2 = "com.lht.ice.handler";
        Set<BeanDefinition> handlerSet = getBeanDefinitions(packageName2, ProtoHandler.class);
        for (BeanDefinition handler : handlerSet) {
            try {
                Class<ProtoHandler> handlerClass = (Class<ProtoHandler>) Class.forName(handler.getBeanClassName());
                for (Method method : handlerClass.getMethods()) {
                    if (!method.isBridge() && method.getName().equals("progress")) {
                        Class<?> parameterType = method.getParameterTypes()[1];
                        Integer opCode = codeToProto.inverse().get(parameterType.getName());
                        ProtoHandler bean = applicationContext.getBean(handlerClass);
                        handlerMap.put(opCode, bean);
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static Set<BeanDefinition> getBeanDefinitions(String packageName, Class<?> targetType) {
        ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
        TypeFilter tf = new AssignableTypeFilter(targetType); // 找到所有类，如果你需要找到特定父类/接口的子类，这里可以替换
        provider.addIncludeFilter(tf);
        Set<BeanDefinition> beans = provider.findCandidateComponents(packageName);
        return beans;
    }

    public static ProtoHandler<?, ?> getHandler(int opcode) {
        return handlerMap.get(opcode);
    }

    public static Message getMessage(int opcode, ByteString data) {
        Method method = codeToParseFrom.get(opcode);
        try {
            return (Message) method.invoke(null, data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}