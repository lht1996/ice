package com.lht.ice.net;

import com.google.protobuf.Message;
import com.lht.ice.data.Anchor;
import com.lht.ice.net.proto.Common;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class WebSocketFrameHandler extends SimpleChannelInboundHandler<WebSocketFrame> {
    @Getter
    private static final Map<String, Anchor> anchorMap = new ConcurrentHashMap<>();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame frame) throws Exception {
        // 判断是否是二进制消息
        if (frame instanceof BinaryWebSocketFrame) {
            ByteBuf content = frame.content();
            Common.ReqMsg reqMsg = Common.ReqMsg.parseFrom(ByteBufUtil.getBytes(content));
            System.out.println(reqMsg);
            Anchor anchor = anchorMap.get(ctx.channel().id().asLongText());
            int opCode = reqMsg.getOpCode();
            if (opCode != 1004 && !anchor.isUseAble()) {
                return;
            }
            ProtoHandler handler = HandlerRegister.getHandler(opCode);
            Message req = HandlerRegister.getMessage(opCode, reqMsg.getData());
            log.info("req:" + req.toString());
            Common.RespMsg.Builder respProto = Common.RespMsg.newBuilder();
            try {
                Message resp = handler.progress(anchor, req);
                respProto.setErrorCode(Common.ErrorCode.Success);
                Integer respOpCode = resp.getDescriptorForType().getOptions().getExtension(Common.opcode);
                respProto.setOpCode(respOpCode);
                respProto.setData(resp.toByteString());

            } catch (Exception e) {
                log.error(Arrays.toString(e.getStackTrace()));
                respProto.setErrorCode(Common.ErrorCode.Fail);
                respProto.setErrorInfo(e.getMessage());
            }
            Common.RespMsg build = respProto.build();
        } else {
            String message = "unsupported frame type: " + frame.getClass().getName();
            throw new UnsupportedOperationException(message);
        }
    }

    // 当web客户端连接后，触发方法
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        // id 表示唯一的值，LongText 是唯一的，ShortText 不是唯一的
        System.out.println("handlerAdded 被调用" + ctx.channel().id().asLongText());
        System.out.println("handlerAdded 被调用" + ctx.channel().id().asShortText());
        Anchor anchor = new Anchor();
//        anchor.setChannel(ctx);
        anchor.setLastHeartbeatTime(System.currentTimeMillis());
        anchorMap.put(ctx.channel().id().asLongText(), anchor);
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {

        System.out.println("handlerRemoved 被调用" + ctx.channel().id().asLongText());
        anchorMap.remove(ctx.channel().id().asLongText());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println("异常发生 " + cause.getMessage());
        ctx.close(); // 关闭连接
    }
}