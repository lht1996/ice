package com.lht.ice.net;

import com.google.protobuf.Message;
import com.lht.ice.data.Anchor;
import com.lht.ice.net.proto.Common;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class WebSocket extends BinaryWebSocketHandler {
    @Getter
    private static final Map<String, Anchor> anchorMap = new ConcurrentHashMap<>();

    @Override
    public void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws IOException {
        if (!session.isOpen()) {
            return;
        }
        Anchor anchor = anchorMap.get(session.getId());
        Common.ReqMsg reqMsg = Common.ReqMsg.parseFrom(message.getPayload());
        int opCode = reqMsg.getOpCode();
        if (opCode != 1004 && !anchor.isUseAble()) {
            return;
        }
        ProtoHandler handler = HandlerRegister.getHandler(opCode);
        Message req = HandlerRegister.getMessage(opCode, reqMsg.getData());
        log.info("req:" + req.toString());
        Common.RespMsg.Builder respProto = Common.RespMsg.newBuilder();
        try {
            Message resp = handler.progress(anchor, req);
            respProto.setErrorCode(Common.ErrorCode.Success);
            Integer respOpCode = resp.getDescriptorForType().getOptions().getExtension(Common.opcode);
            respProto.setOpCode(respOpCode);
            respProto.setData(resp.toByteString());
            log.info("resp:" + resp);
        } catch (Exception e) {
            log.error(Arrays.toString(e.getStackTrace()));
            respProto.setErrorCode(Common.ErrorCode.Fail);
            respProto.setErrorInfo(e.getMessage());
        }
        SendMsg sendMsg = HandlerRegister.getApplicationContext().getBean(SendMsg.class);
        sendMsg.sendMsg(anchor.getSession(), respProto.build());
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
        Anchor anchor = new Anchor();
        anchor.setSession(session);
        anchor.setLastHeartbeatTime(System.currentTimeMillis());
        anchorMap.put(session.getId(), anchor);
        log.info("新链接" + session.getId());
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
        anchorMap.remove(session.getId());
        log.info("关闭链接" + session.getId());
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        log.error("session:" + session + exception.getMessage());
    }
}