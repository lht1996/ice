package com.lht.ice.net;

import com.google.protobuf.Message;
import com.lht.ice.data.Anchor;

/**
 * @author 乐浩天
 */
public interface ProtoHandler<P extends Message, R extends Message> {
    R progress(Anchor anchor, P message);
}
