package com.lht.ice.net;

import com.google.protobuf.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.concurrent.ThreadPoolExecutor;

@Component
@Slf4j
public class SendMsg {
    @Bean("msgExecutor") // bean的名称，默认为首字母小写的方法名
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(1);
        executor.setMaxPoolSize(1);
        executor.setQueueCapacity(500);
        executor.setKeepAliveSeconds(10);
        executor.setThreadNamePrefix("msg-thread");
        // 线程池对拒绝任务的处理策略
        // CallerRunsPolicy：由调用线程（提交任务的线程）处理该任务
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 初始化
        executor.initialize();
        return executor;
    }

    @Async("msgExecutor")
    public void sendMsg(WebSocketSession session, Message message) {
        if (session != null && session.isOpen()) {
            try {
                log.info("sendMsg:" + message.toString());
                session.sendMessage(new BinaryMessage(message.toByteArray()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
