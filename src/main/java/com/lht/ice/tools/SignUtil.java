package com.lht.ice.tools;

import java.security.MessageDigest;
import java.util.*;

/**
 * @author 乐浩天
 */
public class SignUtil {


    /**
     * @param header  = {
     *                "x-nonce-str": "123456",
     *                "x-timestamp": "456789",
     *                "x-roomid":    "268",
     *                "x-msg-type":  "live_gift",
     *                }
     * @param bodyStr = "abc123你好"
     * @param secret  = "123abc"
     * @return PDcKhdlsrKEJif6uMKD2dw==
     */
    private static String signature(Map<String, String> header, String bodyStr, String secret) throws Exception {
        List<String> keyList = new ArrayList<>(4);
        header.forEach((key, val) -> keyList.add(key));
        keyList.sort(String::compareTo);

        List<String> kvList = new ArrayList<>(4);
        for (String key : keyList) {
            kvList.add(key + "=" + header.get(key));
        }
        String urlParams = String.join("&", kvList);
        String rawData = urlParams + bodyStr + secret;
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(rawData.getBytes());
        return Base64.getEncoder().encodeToString(md.digest());
    }

    public static boolean signatureCheck(Map<String, String> header, String bodyStr) {
        String signature = header.get("x-signature");
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("x-nonce-str", header.get("x-nonce-str"));
        hashMap.put("x-timestamp", header.get("x-timestamp"));
        hashMap.put("x-roomid", header.get("x-roomid"));
        hashMap.put("x-msg-type", header.get("x-msg-type"));
        try {
            String signature1 = signature(hashMap, bodyStr, "lht");
            return signature.equals(signature1);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
