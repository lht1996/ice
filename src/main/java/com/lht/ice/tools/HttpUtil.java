package com.lht.ice.tools;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSONObject;
import com.lht.ice.config.Json;
import com.lht.ice.ticker.GetTokenTicker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class HttpUtil {
    // 创建一个 RestTemplate 实例
    private static final RestTemplate REST_TEMPLATE = new RestTemplate();

    public static RestTemplate getRestTemplate() {
        return REST_TEMPLATE;
    }

    public static <T> ResponseEntity<T> request(String url, HttpMethod method, Class<T> responseType) {
        return REST_TEMPLATE.exchange(url, method, null, responseType);
    }

    // 添加了更多的参数的版本，方便进行进一步定制
    public static <T> ResponseEntity<T> request(String url, HttpMethod method,
                                                RequestCallback requestCallback,
                                                ResponseExtractor<ResponseEntity<T>> responseExtractor) {
        return REST_TEMPLATE.execute(url, method, requestCallback, responseExtractor);
    }

    // 方便发起 GET 请求的方法
    public static <T> ResponseEntity<T> get(String url, Class<T> responseType) {
        return request(url, HttpMethod.GET, responseType);
    }

    // 方便发起 POST 请求的方法
    public static <T> ResponseEntity<T> post(String url, Map<String, Object> params, Class<T> responseType) {
        // 创建请求头
        HttpHeaders headers = new HttpHeaders();
        if (GetTokenTicker.getAccessToken() != null) {
            headers.put("X-Token", List.of(GetTokenTicker.getAccessToken()));
            headers.put("x-token", List.of(GetTokenTicker.getAccessToken()));
            headers.put("access-token", List.of(GetTokenTicker.getAccessToken()));
        }
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Map<String, Object>> requestEntity = new HttpEntity<>(params, headers);
        log.info("req+" + requestEntity);
        ResponseEntity<T> response = REST_TEMPLATE.exchange(url, HttpMethod.POST, requestEntity, responseType);
        log.info("resp+" + response);
        return response;
    }

    public static <T> ResponseEntity<T> post(String url, String json, Class<T> responseType) {
        // 创建请求头
        HttpHeaders headers = new HttpHeaders();
        if (GetTokenTicker.getAccessToken() != null) {
            headers.put("X-Token", List.of(GetTokenTicker.getAccessToken()));
            headers.put("access-token", List.of(GetTokenTicker.getAccessToken()));
        }
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);
        log.info("req+" + requestEntity);
        ResponseEntity<T> response = REST_TEMPLATE.exchange(url, HttpMethod.POST, requestEntity, responseType);
        log.info("resp+" + response);
        return response;
    }

    public static JSONObject huToolPost(String url, Map<String, Object> params) {
        Map<String, String> headers = new HashMap();
        headers.put("x-token", GetTokenTicker.getAccessToken());
        headers.put("access-token", GetTokenTicker.getAccessToken());
        headers.put("content-type", "application/json");
        cn.hutool.http.HttpRequest httpRequest = cn.hutool.http.HttpUtil.createPost(url);
        cn.hutool.http.HttpRequest body = ((HttpRequest) httpRequest.addHeaders(headers)).body(Json.objectToJson(params));
        HttpResponse response = body.execute();
        return (JSONObject) JSONObject.parseObject(response.body(), JSONObject.class);
    }
}