package com.lht.ice.handler;

import com.lht.ice.data.Anchor;
import com.lht.ice.net.ProtoHandler;
import com.lht.ice.net.proto.Login;
import org.springframework.stereotype.Component;

/**
 * @author 乐浩天
 */
@Component
public class HeartbeatHandler implements ProtoHandler<Login.HeartbeatReq, Login.HeartbeatResp> {

    public Login.HeartbeatResp progress(Anchor anchor, Login.HeartbeatReq req) {
        long now = System.currentTimeMillis();
        anchor.setLastHeartbeatTime(now);
        return Login.HeartbeatResp.newBuilder().setTimestamp(now).build();
    }
}
