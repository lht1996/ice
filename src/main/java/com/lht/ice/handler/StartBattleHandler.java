package com.lht.ice.handler;

import com.lht.ice.data.Anchor;
import com.lht.ice.data.AnchorRoom;
import com.lht.ice.net.ProtoHandler;
import com.lht.ice.net.proto.Login;
import com.lht.ice.net.proto.Room;
import com.lht.ice.service.RedisKey;
import com.lht.ice.ticker.BattleTicker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @author 乐浩天
 */
@Component
@Slf4j
public class StartBattleHandler implements ProtoHandler<Login.StartBattleReq, Login.StartBattleResp> {
    @Value("${appId}")
    private String appId;
    @Autowired
    private BattleTicker battleTicker;

    @Autowired
    private StringRedisTemplate redisTemplate;

    public Login.StartBattleResp progress(Anchor anchor, Login.StartBattleReq req) {
        log.info("anchor" + anchor.toString());
        AnchorRoom room = battleTicker.getRoomMap().get(anchor.getRoomId());
        if (room == null) {
            throw new RuntimeException("房间未创建");
        }
        Long roundId = redisTemplate.opsForValue().increment(RedisKey.RoomRound.name(), 1);
        if (roundId == null) {
            throw new RuntimeException("获取roundId失败");
        }
        if (room.getStatus() != Room.RoomStatus.ready) {
            throw new RuntimeException("房间状态错误");
        }
        room.start(req.getGameType(), roundId);
        room.getPlayerMap().clear();
        return Login.StartBattleResp.newBuilder().build();
    }
}
