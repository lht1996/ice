package com.lht.ice.handler;

import com.lht.ice.config.Json;
import com.lht.ice.data.*;
import com.lht.ice.net.ProtoHandler;
import com.lht.ice.net.proto.Login;
import com.lht.ice.net.proto.Room;
import com.lht.ice.service.RedisKey;
import com.lht.ice.ticker.BattleTicker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @author 乐浩天
 */
@Component
public class GmHandler implements ProtoHandler<Login.GmReq, Login.GmResp> {
    @Autowired
    private BattleTicker battleTicker;
    @Autowired
    private StringRedisTemplate redisTemplate;

    public Login.GmResp progress(Anchor anchor, Login.GmReq req) {
        Login.GmResp.Builder builder = Login.GmResp.newBuilder();
        AnchorRoom room = battleTicker.getRoomMap().get(anchor.getRoomId());
        if (room == null) {
            throw new RuntimeException("房间未创建");
        }
        String[] commands = req.getCommand().split(" ");
        switch (commands[0]) {
            case "comment": {
                LiveComment liveComment = new LiveComment();
                liveComment.setSec_openid(commands[1]);
                liveComment.setNickname(commands[1]);
                liveComment.setAvatar_url("111");
                liveComment.setContent(commands[2]);
                room.comment(liveComment);
                String playerId = commands[1];
                String outJson = (String) redisTemplate.opsForHash().get(RedisKey.Player.name(), playerId);
                PlayerData playerData = null;
                if (outJson != null) {
                    playerData = Json.jsonToObject(outJson, PlayerData.class);
                }
                if (playerData == null) {
                    playerData = new PlayerData();
                    playerData.setOpenId(playerId);
                    playerData.setWinNum(0);
                }
                Player player = room.getPlayerMap().get(playerId);
                player.setWinNum(playerData.getWinNum());
                player.setBear(playerData.getTotalBear());
                playerData.setIcon(liveComment.getAvatar_url());
                playerData.setName(liveComment.getNickname());
                String json = Json.objectToJson(playerData);
                redisTemplate.opsForHash().put(RedisKey.Player.name(), playerId, json);
                break;
            }
            case "like": {
                LiveLike liveLike = new LiveLike();
                liveLike.setSec_openid(commands[1]);
                liveLike.setLike_num(Integer.parseInt(commands[2]));
                room.like(liveLike);
                break;
            }
            case "gift": {
                LiveGift liveGift = new LiveGift();
                liveGift.setSec_openid(commands[1]);
                Room.ShipType shipType = Room.ShipType.forNumber(Integer.parseInt(commands[2]));
                MetaGift metaGift = Json.shipTypeToMeta.get(shipType);
                liveGift.setSec_gift_id(metaGift.getId());
                liveGift.setGift_num(Integer.parseInt(commands[3]));
                room.gift(liveGift);
                break;
            }
        }
        return builder.build();
    }
}
