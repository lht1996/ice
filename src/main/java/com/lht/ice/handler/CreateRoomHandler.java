package com.lht.ice.handler;

import com.alibaba.fastjson.JSONObject;
import com.lht.ice.config.Json;
import com.lht.ice.data.Anchor;
import com.lht.ice.data.AnchorRoom;
import com.lht.ice.data.MsgType;
import com.lht.ice.data.resp.GetRoomInfoResp;
import com.lht.ice.data.resp.StartPushResp;
import com.lht.ice.data.resp.TopGiftResp;
import com.lht.ice.net.ProtoHandler;
import com.lht.ice.net.proto.Login;
import com.lht.ice.ticker.BattleTicker;
import com.lht.ice.ticker.GetTokenTicker;
import com.lht.ice.tools.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;

/**
 * @author 乐浩天
 */
@Component
@Slf4j
public class CreateRoomHandler implements ProtoHandler<Login.CreateRoomReq, Login.CreateRoomResp> {

    @Autowired
    private BattleTicker battleTicker;

    public Login.CreateRoomResp progress(Anchor anchor, Login.CreateRoomReq req) {
        Map<String, Object> params = new HashMap<>();
        if (req.getToken().equals("test")) {
            anchor.setRoomId(1000);
            anchor.setAnchorOpenId("1000");
            anchor.setAvatarUrl("test");
            anchor.setNickName("test");
            anchor.setUseAble(true);
            AnchorRoom room = new AnchorRoom(anchor);
            battleTicker.getRoomMap().put(anchor.getRoomId(), room);
            return Login.CreateRoomResp.newBuilder().setRoomId(1000).build();
        }
        params.put("token", req.getToken());
        String url = "https://webcast.bytedance.com/api/webcastmate/info";
        ResponseEntity<GetRoomInfoResp> post = HttpUtil.post(url, params, GetRoomInfoResp.class);
        if (post.getBody() == null || post.getBody().getData() == null) {
            throw new RuntimeException("获取房间信息失败:" + post);
        }
        GetRoomInfoResp.Data.Info info = post.getBody().getData().getInfo();
        anchor.setRoomId(info.getRoomId());
        anchor.setAnchorOpenId(info.getAnchorOpenId());
        anchor.setAvatarUrl(info.getAvatarUrl());
        anchor.setNickName(info.getNickName());
        anchor.setUseAble(true);
        AnchorRoom anchorRoom = battleTicker.getRoomByIdAnchorId(anchor.getAnchorOpenId());
        if (anchorRoom != null) {
            battleTicker.getRoomMap().remove(anchorRoom.getAnchor().getRoomId());
        }
        createRoom(anchor);
        return Login.CreateRoomResp.newBuilder().setRoomId(info.getRoomId()).build();
    }

    private void createRoom(Anchor anchor) {
        startPush(anchor.getRoomId(), MsgType.comment);
        startPush(anchor.getRoomId(), MsgType.gift);
        startPush(anchor.getRoomId(), MsgType.like);
        AnchorRoom room = new AnchorRoom(anchor);
        battleTicker.getRoomMap().put(anchor.getRoomId(), room);

//        top1(room);
        top2(room);
    }

    private void top2(AnchorRoom room) {
        Map<String, Object> params = new HashMap();
        params.put("room_id", String.valueOf(room.getRoomId()));
        params.put("app_id", this.appId);
        params.put("sec_gift_id_list", Json.giftIdToMeta.keySet());
        String url = "https://webcast.bytedance.com/api/gift/top_gift";
        JSONObject res = HttpUtil.huToolPost(url, params);
        log.info("礼物置顶result={}", res);
    }

    private void top1(AnchorRoom room) {
        RestTemplate restTemplate = HttpUtil.getRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("x-token", GetTokenTicker.getAccessToken());
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("room_id", room.getRoomId());
        params.put("app_id", appId);
        params.put("sec_gift_id_list", Json.objectToJson(List.of(Json.giftIdToMeta.keySet())));
        log.info("params: {}", params);
        log.info("headers: {}", headers);
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(params, headers);
        log.info("entity: {}", entity);
        ResponseEntity<TopGiftResp> response = restTemplate.exchange(
                "https://webcast.bytedance.com/api/gift/top_gift",
                HttpMethod.POST,
                entity,
                TopGiftResp.class
        );
        log.info("top1 response: {}", response);
    }

    public void top3(AnchorRoom room) {
        RestTemplate restTemplate = new RestTemplate();

        // Set headers
        HttpHeaders headers = new HttpHeaders();
        headers.set("x-token", GetTokenTicker.getAccessToken());
        headers.setContentType(MediaType.APPLICATION_JSON);
        String[] secGiftIdList = Json.giftIdToMeta.keySet().toArray(new String[0]);
        // Create request body
        String requestBody = String.format("{\"room_id\":\"%s\",\"app_id\":\"%s\",\"sec_gift_id_list\":[\"%s\",\"%s\"]}",
                room.getRoomId(), appId, secGiftIdList[0], secGiftIdList[1]);

        // Create HttpEntity with headers and request body
        HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);

        // Send POST request
        ResponseEntity<String> responseEntity = restTemplate.exchange("https://webcast.bytedance.com/api/gift/top_gift", HttpMethod.POST, requestEntity, String.class);

        // Print response
        System.out.println("top3 Response: " + responseEntity.getBody());
    }

    public void top4(AnchorRoom room) {
        RestTemplate restTemplate = new RestTemplate();

        // Set headers
        HttpHeaders headers = new HttpHeaders();
        headers.set("x-token", GetTokenTicker.getAccessToken());
        headers.setContentType(MediaType.APPLICATION_JSON);
        String[] secGiftIdList = Json.giftIdToMeta.keySet().toArray(new String[0]);
        // Create request body
        Map<String, Object> requestBodyMap = new HashMap<>();
        requestBodyMap.put("room_id", room.getRoomId());
        requestBodyMap.put("app_id", appId);
        requestBodyMap.put("sec_gift_id_list", Arrays.asList(secGiftIdList));


        // Create HttpEntity with headers and request body
        HttpEntity<Map<String, Object>> requestEntity = new HttpEntity<>(requestBodyMap, headers);

        // Send POST request
        ResponseEntity<String> responseEntity = restTemplate.exchange("https://webcast.bytedance.com/api/gift/top_gift", HttpMethod.POST, requestEntity, String.class);

        // Print response
        System.out.println("top4 Response: " + responseEntity.getBody());
    }

    @Value("${appId}")
    private String appId;

    private void startPush(long roomId, MsgType msgType) {
        Map<String, Object> params = new HashMap<>();
        params.put("appid", appId);
        params.put("roomid", String.valueOf(roomId));
        params.put("msg_type", msgType.getName());
        String url = "https://webcast.bytedance.com/api/live_data/task/start";
        ResponseEntity<StartPushResp> post = HttpUtil.post(url, params, StartPushResp.class);
        if (post.getBody() == null || post.getBody().getData() == null || post.getBody().getErrorNumber() != 0) {
            throw new RuntimeException("开启推送失败");
        }
    }
}
