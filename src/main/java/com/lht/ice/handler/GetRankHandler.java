package com.lht.ice.handler;

import com.lht.ice.config.Json;
import com.lht.ice.data.Anchor;
import com.lht.ice.data.PlayerData;
import com.lht.ice.net.ProtoHandler;
import com.lht.ice.net.proto.Room;
import com.lht.ice.service.RedisKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author 乐浩天
 */
@Component
public class GetRankHandler implements ProtoHandler<Room.GetRankReq, Room.GetRankResp> {
    @Autowired
    private StringRedisTemplate redisTemplate;

    public Room.GetRankResp progress(Anchor anchor, Room.GetRankReq req) {
        int page = req.getPage();
        int pageSize = req.getPageSize();
        String redisKey = RedisKey.Rank.name() + req.getRankType().name();
        switch (req.getRankType()) {
            case week:
                long week = System.currentTimeMillis() / (1000 * 60 * 60 * 24 * 7);
                redisKey += week;
                break;
        }
        Set<ZSetOperations.TypedTuple<String>> typedTuples = redisTemplate.opsForZSet()
                .reverseRangeWithScores(redisKey, (long) (page - 1) * pageSize, (long) page * pageSize - 1);
        Room.GetRankResp.Builder builder = Room.GetRankResp.newBuilder();
        int rank = (page - 1) * pageSize + 1;
        if (typedTuples != null) {
            for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
                String outJson = (String) redisTemplate.opsForHash().get(RedisKey.Player.name(), typedTuple.getValue());
                if (outJson != null) {
                    PlayerData playerData = Json.jsonToObject(outJson, PlayerData.class);
                    builder.addRanks(Room.RankInfo.newBuilder()
                            .setPlayerId(typedTuple.getValue())
                            .setIcon(playerData.getIcon())
                            .setName(playerData.getName())
                            .setScore(typedTuple.getScore().intValue())
                            .setRank(rank)
                            .build());
                    rank++;
                }
            }
        }
        return builder.build();
    }
}
