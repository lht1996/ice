package com.lht.ice.ticker;

import com.lht.ice.config.Json;
import com.lht.ice.data.*;
import com.lht.ice.net.SendMsg;
import com.lht.ice.net.proto.Common;
import com.lht.ice.net.proto.Room;
import com.lht.ice.service.RedisKey;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 乐浩天
 */
@Getter
@Component
@Slf4j
public class BattleTicker {
    @Autowired
    private SendMsg sendMsg;

    @Autowired
    private StringRedisTemplate redisTemplate;

    private final Map<Long, AnchorRoom> roomMap = new ConcurrentHashMap<>();

    @Scheduled(cron = "* * * * * *")
    public void reqAccessToken() {
        for (AnchorRoom room : roomMap.values()) {
            if (room.getStatus() == Room.RoomStatus.start) {
                Room.RoomPush roomPush = room.round();
                if (roomPush == null) {
                    return;
                }
                Integer opCode = roomPush.getDescriptorForType().getOptions().getExtension(Common.opcode);
                Common.RespMsg respProto = Common.RespMsg.newBuilder()
                        .setOpCode(opCode)
                        .setData(roomPush.toByteString())
                        .build();
                log.info("roomPush" + roomPush);
                sendMsg.sendMsg(room.getAnchor().getSession(), respProto);
            } else if (room.getStatus() == Room.RoomStatus.leftWin
                    || room.getStatus() == Room.RoomStatus.rightWin) {
                // 有一方胜利战斗结束
                winSettle(room);
                room.setStatus(Room.RoomStatus.ready);
            }
        }
    }

    public AnchorRoom getRoomByIdAnchorId(String openId) {
        for (AnchorRoom room : roomMap.values()) {
            if (room.getAnchor().getAnchorOpenId().equals(openId)) {
                return room;
            }
        }
        return null;
    }

    /**
     * 增加北极熊概念此为可以争抢的累计产物，不会销毁。
     * 2 每局爱心值大于>500的胜利方玩家可以获得基础北极熊一个，此为系统给熊，系统每天每人给熊不超过10。
     * 3 每局每个玩家小于10只熊拿出50%，进入可被争抢的奖励池子
     * 每局每个玩家大于10只熊拿出30%，进入可被争抢的池子
     * 胜利方拿走失败方被争抢的奖励池子90%，剩下10%被系统回收，每个玩家获得北极熊数量按照本局每个玩家爱心值方爱心总值。
     * 4 每局在排行榜界面显示每个玩家获得多少只北极熊
     */
    public void winSettle(AnchorRoom room) {
        Side winSide = room.getStatus() == Room.RoomStatus.leftWin ? room.getLeftSide() : room.getRightSide();
        Side failSide = room.getStatus() == Room.RoomStatus.rightWin ? room.getLeftSide() : room.getRightSide();
        List<String> winList = winSide.getShipMap().values().stream()
                .map(Ship::getPlayerId).distinct().toList();
        List<String> failList = failSide.getShipMap().values().stream()
                .map(Ship::getPlayerId).distinct().toList();
        Map<String, Integer> bearMap = new HashMap<>();
        int failBear = 0;
        for (String playerId : failList) {
            String outJson = (String) redisTemplate.opsForHash().get(RedisKey.Player.name(), playerId);
            if (outJson != null) {
                PlayerData playerData = Json.jsonToObject(outJson, PlayerData.class);
                if (playerData != null) {
                    int reduceBear = 0;
                    playerData.setWinNum(0);
                    int totalBear = playerData.getTotalBear();
                    if (totalBear <= 10) {
                        reduceBear = (int) (totalBear * 0.5);
                    } else {
                        reduceBear = (int) (totalBear * 0.3);
                    }
                    playerData.setTotalBear(playerData.getTotalBear() - reduceBear);
                    String json = Json.objectToJson(playerData);
                    redisTemplate.opsForHash().put(RedisKey.Player.name(), playerId, json);
                    failBear += reduceBear;
                    bearMap.put(playerId, reduceBear);
                }
            }
        }
        // 争抢奖励池子熊数量大于10只，系统回收10%
        if (failBear > 10) {
            failBear = (int) (failBear * 0.9);
        }
        int addBear = failBear / winList.size();
        for (String playerId : winList) {
            double contribute = room.getPlayerMap().get(playerId).getContribute();
            String outJson = (String) redisTemplate.opsForHash().get(RedisKey.Player.name(), playerId);
            if (outJson != null) {
                PlayerData playerData = Json.jsonToObject(outJson, PlayerData.class);
                if (playerData != null) {
                    playerData.setWinNum(playerData.getWinNum() + 1);
                    int bear = addBear;
                    if (contribute >= 500 && playerData.getDailyBear() < 10) {
                        playerData.setDailyBear(playerData.getDailyBear() + 1);
                        bear += 1;
                    }
                    playerData.setTotalBear(playerData.getTotalBear() + bear);
                    bearMap.put(playerId, bear);
                    String json = Json.objectToJson(playerData);
                    redisTemplate.opsForHash().put(RedisKey.Player.name(), playerId, json);
                }
            }
        }
        Room.BattleEndPush.Builder builder = Room.BattleEndPush.newBuilder();
        // 获取当前时间是第几周
        long week = System.currentTimeMillis() / (1000 * 60 * 60 * 24 * 7);
        String redisKey = RedisKey.Rank.name() + Room.RankType.week.name() + week;
        Map<String, Long> oldRankMap = new HashMap<>();
        for (Player player : room.getPlayerMap().values()) {
            Long oldRank = redisTemplate.opsForZSet().rank(redisKey, player.getOpenId());
            oldRankMap.put(player.getOpenId(), oldRank == null ? 0 : oldRank);
        }
        Map<String, Integer> soreMap = new HashMap<>();
        for (Player player : room.getPlayerMap().values()) {
            Double sore = redisTemplate.opsForZSet().incrementScore(redisKey, player.getOpenId(), -player.getContribute());
            soreMap.put(player.getOpenId(), (int) (sore == null ? player.getContribute() : -sore));
        }
        for (Player player : room.getPlayerMap().values()) {
            Long rank = redisTemplate.opsForZSet().rank(redisKey, player.getOpenId());
            if (rank == null) {
                rank = 0L;
            }
            rank++;
            int rankUp = oldRankMap.get(player.getOpenId()).intValue() - rank.intValue();
            Room.PlayerRank.Builder member = Room.PlayerRank.newBuilder();
            member.setPlayerId(player.getOpenId());
            member.setNickname(player.getNickName());
            member.setAvatar(player.getAvatarUrl());
            member.setWinNum(player.getWinNum());
            member.setScore((int) player.getContribute());
            member.setTotalScore(soreMap.get(player.getOpenId()));
            member.setRank(rank.intValue());
            member.setRankUp(rankUp);
            Integer bear = bearMap.get(player.getOpenId());
            member.setBear(bear == null ? 0 : bear);
            builder.addRanks(member.build());
        }
        Room.BattleEndPush endPush = builder.build();
        Integer opCode = endPush.getDescriptorForType().getOptions().getExtension(Common.opcode);
        log.info("endPush" + endPush);
        Common.RespMsg respProto = Common.RespMsg.newBuilder()
                .setOpCode(opCode)
                .setData(endPush.toByteString())
                .build();
        sendMsg.sendMsg(room.getAnchor().getSession(), respProto);
    }
}
