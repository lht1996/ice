package com.lht.ice.ticker;

import com.lht.ice.data.Anchor;
import com.lht.ice.net.WebSocket;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author 乐浩天
 */
@Getter
@Component
@Slf4j
public class SocketTicker {

    @Scheduled(cron = "* * * * * *")
    public void heartbeat() {
        long now = System.currentTimeMillis();
        for (Anchor anchor : WebSocket.getAnchorMap().values()) {
            if (now - anchor.getLastHeartbeatTime() > 1000 * 30) {
                try {
                    anchor.getSession().close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

}
