package com.lht.ice.ticker;

import com.lht.ice.data.resp.AccessTokenResp;
import com.lht.ice.tools.HttpUtil;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author 乐浩天
 */
@Component
@Slf4j
public class GetTokenTicker {
    @Value("${appId}")
    private String appId;
    @Value("${appSecret}")
    private String appSecret;

    @Getter
    private static String accessToken;

    @PostConstruct
    public void init() {
        reqAccessToken();
    }

    /**
     * 每小时获取一次token
     */
    @Scheduled(cron = "0 0 * * * ?")
    public void reqAccessToken() {
        // 创建请求参数
        Map<String, Object> params = new HashMap<>();
        params.put("appid", appId);
        params.put("secret", appSecret);
        params.put("grant_type", "client_credential");
        String url = "https://developer.toutiao.com/api/apps/v2/token";
        ResponseEntity<AccessTokenResp> post = HttpUtil.post(url, params, AccessTokenResp.class);
        if (post.getBody() == null) {
            log.error("获取token失败");
            return;
        }
        if (post.getBody().getErrorNumber() != 0 || !Objects.equals(post.getBody().getErrorTips(), "success")) {
            log.error("获取token失败，错误码：{}，错误信息：{}", post.getBody().getErrorNumber(), post.getBody().getErrorTips());
            return;
        }
        accessToken = post.getBody().getData().getAccessToken();
        log.info("获取到的token为：{}", post.getBody());
    }

}
