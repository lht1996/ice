package com.lht.ice.data.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TopGiftResp {

    @JsonProperty("err_no")
    private int err_no;

    @JsonProperty("err_msg")
    private String err_msg;

    @JsonProperty("logid")
    private String logid;

    private Data data;

    @lombok.Data
    public static class Data {

        @JsonProperty("success_top_gift_id_list")
        private String[] success_top_gift_id_list;
    }
}