package com.lht.ice.data.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class StartPushResp {

    @JsonProperty("err_no")
    private int errorNumber;

    @JsonProperty("err_tips")
    private String errorTips;

    @JsonProperty("logid")
    private String logId;

    private Data data;

    @lombok.Data
    public static class Data {

        @JsonProperty("task_id")
        private String taskId;
    }
}