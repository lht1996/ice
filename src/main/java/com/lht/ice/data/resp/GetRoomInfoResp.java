package com.lht.ice.data.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author 乐浩天
 */
@Data
public class GetRoomInfoResp {
    @JsonProperty("errcode")
    private int errCode;

    @JsonProperty("errmsg")
    private String errMsg;

    private Data data;

    @lombok.Data
    public static class Data {
        private Info info;

        @lombok.Data
        public static class Info {

            @JsonProperty("room_id")
            private long roomId;

            @JsonProperty("anchor_open_id")
            private String anchorOpenId;

            @JsonProperty("avatar_url")
            private String avatarUrl;

            @JsonProperty("nick_name")
            private String nickName;

        }

    }
}
