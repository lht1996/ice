package com.lht.ice.data.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AccessTokenResp {

    @JsonProperty("err_no")
    private int errorNumber;

    @JsonProperty("err_tips")
    private String errorTips;

    private Data data;

    @lombok.Data
    public static class Data {

        @JsonProperty("access_token")
        private String accessToken;

        @JsonProperty("expires_in")
        private int expiresIn;

        @JsonProperty("expiresAt")
        private long expiresAt;

    }
}