package com.lht.ice.data;

import lombok.Getter;

/**
 * @author 乐浩天
 */
@Getter
public enum MsgType {
    comment(1, "live_comment"),
    gift(2, "live_gift"),
    like(3, "live_like"),
    fansClub(4, "live_fansclub");

    private final int id;
    private final String name;

    MsgType(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
