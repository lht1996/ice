package com.lht.ice.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.SneakyThrows;

import java.util.ArrayList;

@Data
public class MetaGift {
    private String id;
    private int type;
    private int pulling;
    private int duration;

    public MetaGift(String id, int type, int pulling, int duration) {
        this.id = id;
        this.type = type;
        this.pulling = pulling;
        this.duration = duration;
    }

    @SneakyThrows
    public static void main(String[] args) {
        ArrayList<MetaGift> metaGifts = new ArrayList<>();
        metaGifts.add(new MetaGift("n1/Dg1905sj1FyoBIQBvmbaDZFBNaKuKZH6zxHkv8Lg5x2cRfrKUTb8gzMs=", 0, 100, 5));
        metaGifts.add(new MetaGift("PJ0FFeaDzXUreuUBZH6Hs+b56Jh0tQjrq0bIrrlZmv13GSAL9Q1hf59fjGk=", 364, 100, 90));
        metaGifts.add(new MetaGift("gx7pmjQfhBaDOG2XkWI2peZ66YFWkCWRjZXpTqb23O/epru+sxWyTV/3Ufs=", 693, 100, 90));
        metaGifts.add(new MetaGift("lkkadLfz7O/a5UR45p/OOCCG6ewAWVbsuzR/Z+v1v76CBU+mTG/wPjqdpfg=", 1393, 100, 120));
        metaGifts.add(new MetaGift("pGLo7HKNk1i4djkicmJXf6iWEyd+pfPBjbsHmd3WcX0lerm2UdnRR7UINvl=", 3640, 100, 150));
        metaGifts.add(new MetaGift("P7zDZzpeO215SpUptB+aURb1+zC14UC9MY1+MHszKoF0p5gzYk8CNEbey60=", 8400, 100, 180));
        ObjectMapper objectMapper = new ObjectMapper();
        String s = objectMapper.writeValueAsString(metaGifts);
        System.out.println(s);
    }
}

