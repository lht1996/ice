package com.lht.ice.data;

import com.lht.ice.net.proto.Room;
import lombok.Data;

@Data
public class Gift {

    private String openId;
    private String giftId;
    private Integer num;

    public Gift(LiveGift liveGift) {
        this.openId = liveGift.getSec_openid();
        this.giftId = liveGift.getSec_gift_id();
        this.num = liveGift.getGift_num();
    }

    public Room.GiftInfo toProto() {
        Room.GiftInfo.Builder gift = Room.GiftInfo.newBuilder();
        gift.setPlayerId(openId);
        gift.setGiftId(giftId);
        gift.setGiftNum(num);
        return gift.build();
    }
}
