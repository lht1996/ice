package com.lht.ice.data;

import lombok.Data;

@Data
public class LiveGift {
    private String msg_id;
    private String sec_openid;
    private String sec_gift_id;
    private Integer gift_num;
    private Integer gift_value;
    private String avatar_url;
    private String nickname;
    private Long timestamp;
    private Boolean test;
}