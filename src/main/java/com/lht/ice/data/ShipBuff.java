package com.lht.ice.data;

import com.lht.ice.net.proto.Room;
import lombok.Data;

@Data
public class ShipBuff {
    private Room.BuffType addType;
    private int num;
    private int round;
    private long sourceShipId;
    private int buffNum;

    public ShipBuff(Room.BuffType addType, int num, int round) {
        this.addType = addType;
        this.num = num;
        this.round = round;
    }

    public boolean round() {
        round--;
        return round <= 0;
    }
}
