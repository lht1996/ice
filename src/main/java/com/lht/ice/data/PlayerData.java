package com.lht.ice.data;

import lombok.Data;

/**
 * @author 乐浩天
 */
@Data
public class PlayerData {
    private String openId;
    private String icon;
    private String name;
    private int winNum;
    private int dailyBear;
    private int totalBear;
}
