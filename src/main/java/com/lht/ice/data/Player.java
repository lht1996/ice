package com.lht.ice.data;

import com.lht.ice.net.proto.Room;
import lombok.Data;

/**
 * @author 乐浩天
 */
@Data
public class Player {
    private String openId;
    private String avatarUrl;
    private String nickName;
    private int winNum;
    private int bear;
    private boolean sideType; // 阵营 true 左边 false 右边
    private Ship baseShip;
    private double contribute;

    public Player(LiveComment comment) {
        this.openId = comment.getSec_openid();
        this.avatarUrl = comment.getAvatar_url();
        this.nickName = comment.getNickname();
    }

    public Room.PlayerInfo toProto() {
        Room.PlayerInfo.Builder player = Room.PlayerInfo.newBuilder();
        player.setPlayerId(openId);
        player.setAvatar(avatarUrl);
        player.setNickname(nickName);
        player.setContinuousWinNum(winNum);
        player.setContribute((int) contribute);
        player.setBear(bear);
        return player.build();
    }
}
