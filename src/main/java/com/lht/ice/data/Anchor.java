package com.lht.ice.data;

import lombok.Data;
import org.springframework.web.socket.WebSocketSession;

/**
 * @author 乐浩天
 */
@Data
public class Anchor {
    private WebSocketSession session;
    private String token;
    private long roomId;
    private String anchorOpenId;
    private String avatarUrl;
    private String nickName;
    private boolean useAble;
    private long lastHeartbeatTime;
}
