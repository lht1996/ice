package com.lht.ice.data;

import com.lht.ice.net.proto.Room;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author 乐浩天
 */
@Data
public class Side {
    private boolean side;
    private Side enemy;
    private Map<Long, Ship> shipMap = new ConcurrentHashMap<>();

    public Ship addShip(long shipId, String playerId, Room.ShipType shipType) {
        Ship ship = new Ship(shipId, playerId, shipType, enemy);
        shipMap.put(ship.getShipId(), ship);
        return ship;
    }

    public Room.SideInfo toProto() {
        int pullingBase = getPullingBase();
        int pullingAdd = getPullingAdd();
        Room.SideInfo.Builder builder = Room.SideInfo.newBuilder();
        builder.setSideType(side);
        builder.setPulling(pullingBase + pullingAdd);
        builder.setPullingAdd(pullingAdd);
        builder.setFog(checkFog());
        shipMap.values().stream()
                .sorted((o1, o2) -> (int) (o2.getShipId() - o1.getShipId()))
                .forEach(ship -> builder.addShips(ship.toProto()));
        return builder.build();
    }

    private boolean checkFog() {
        return shipMap.values().stream()
                .map(Ship::getBuffList)
                .flatMap(List::stream)
                .anyMatch(shipBuff -> shipBuff.getAddType() == Room.BuffType.fog);
    }

    public void round() {
        shipMap.values().removeIf(Ship::round);
    }

    public Map<String, Integer> settle() {
        return shipMap.values().stream()
                .collect(Collectors.groupingBy(Ship::getPlayerId, Collectors.summingInt(Ship::getPulling)));
    }

    public int getPulling() {
        // 求shipList pulling 总和
        return shipMap.values().stream()
                .mapToInt(Ship::getPulling)
                .sum();
    }

    public int getPullingBase() {
        // 求shipList pullingBase 总和
        return shipMap.values().stream()
                .mapToInt(Ship::getPullingBase)
                .sum();
    }


    private int getPullingAdd() {
        // 求shipList pullingAdd 总和
        return shipMap.values().stream()
                .mapToInt(Ship::getPullingAdd)
                .sum();
    }
}
