package com.lht.ice.data;

import lombok.Data;

@Data
public class LiveLike {
    private String msg_id;
    private String sec_openid;
    private Integer like_num;
    private String avatar_url;
    private String nickname;
    private Long timestamp;
}