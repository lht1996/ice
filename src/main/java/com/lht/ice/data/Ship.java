package com.lht.ice.data;

import com.lht.ice.config.Json;
import com.lht.ice.net.proto.Room;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Stream;

@Data
public class Ship {
    private long shipId;
    private String playerId;
    private Room.ShipType shipType;
    private List<ShipBuff> buffList = new CopyOnWriteArrayList<>();
    private Side enemy;
    private int reduceNum;

    public Ship(long shipId, String playerId, Room.ShipType shipType, Side enemy) {
        this.playerId = playerId;
        this.shipId = shipId;
        this.shipType = shipType;
        this.enemy = enemy;
        buffList.add(new ShipBuff(Room.BuffType.gift, 1, Json.shipTypeToMeta.get(shipType).getDuration()));
        switch (shipType) {
            case Level0 -> {
                return;
            }
            case Level4 -> {
                buffList.add(new ShipBuff(Room.BuffType.fog, 1, 2));
            }
            case Level5 -> {
                // 从敌方船中随机选取10个
                List<Ship> shipList = new ArrayList<>(enemy.getShipMap().values());
                Collections.shuffle(shipList);
                Stream<Ship> shipStream;
                if (shipList.size() <= 10) {
                    shipStream = shipList.stream();
                } else {
                    shipStream = shipList.stream().skip(shipList.size() - 10);
                }
                reduceNum = shipStream.mapToInt(ship -> ship.addReduce(shipId)).sum();
            }
        }
    }

    public int addReduce(long shipId) {
        ShipBuff shipBuff = new ShipBuff(Room.BuffType.reduce, 1, 5);
        shipBuff.setSourceShipId(shipId);
        int reduceNum = (int) (getPulling() * 0.05);
        shipBuff.setBuffNum(reduceNum);
        buffList.add(shipBuff);
        return reduceNum;
    }

    public void addLike(int num) {
        if (shipType != Room.ShipType.Level0) {
            return;
        }
        buffList.add(new ShipBuff(Room.BuffType.like, num, 5));
    }

    public void addGift(int num) {
        if (shipType != Room.ShipType.Level0) {
            return;
        }
        buffList.add(new ShipBuff(Room.BuffType.gift, num, Json.shipTypeToMeta.get(shipType).getDuration()));
    }

    public Room.ShipInfo toProto() {
        int round = shipType == Room.ShipType.Level0 ? -1 : buffList.get(0).getRound();
        int totalRound = shipType == Room.ShipType.Level0 ? -1 : Json.shipTypeToMeta.get(shipType).getDuration();
        return Room.ShipInfo.newBuilder()
                .setShipId(shipId)
                .setPlayerId(playerId)
                .setShipType(shipType)
                .setRound(round)
                .setTotalRound(totalRound)
                .setReducePulling(reduceNum)
                .build();
    }

    public boolean round() {
        buffList.removeIf(ShipBuff::round);
        if (shipType == Room.ShipType.Level0) {
            return false;
        } else {
            return buffList.isEmpty();
        }
    }

    public int getPulling() {
        return getPullingBase() + getPullingAdd();
    }


    public int getPullingBase() {
        int pullingAdd = 0;
        if (shipType == Room.ShipType.Level0) {
            pullingAdd += 10;
        }
        return pullingAdd;
    }

    public int getPullingAdd() {
        int pulling = 0;
        for (ShipBuff shipBuff : buffList) {
            switch (shipBuff.getAddType()) {
                case gift -> pulling += Json.shipTypeToMeta.get(shipType).getPulling() * shipBuff.getNum();
                case reduce -> pulling -= shipBuff.getBuffNum() * shipBuff.getNum();
                case like -> pulling += shipBuff.getNum();
            }
        }
        return pulling;
    }
}
