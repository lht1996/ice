package com.lht.ice.data;

import com.lht.ice.config.Json;
import com.lht.ice.net.proto.Room;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author 乐浩天
 */
@Data
@Slf4j
public class AnchorRoom {
    private Anchor anchor;
    private Room.GameType gameType;
    private Room.RoomStatus status;
    private double icebergPos;
    private double speed;
    private long startTime;
    private long endTime;
    private Side leftSide;
    private Side rightSide;
    private Map<String, Player> playerMap = new ConcurrentHashMap<>();

    private List<Gift> giftList = new CopyOnWriteArrayList<>();

    private long roundId;
    private int roundNum;

    private long shipIndex = 1;

    public AnchorRoom(Anchor anchor) {
        this.anchor = anchor;
        this.status = Room.RoomStatus.ready;
    }

    public void start(Room.GameType gameType, long roundId) {
        this.shipIndex = 1;
        this.gameType = gameType;
        this.roundId = roundId;
        this.status = Room.RoomStatus.start;
        this.icebergPos = 0;
        this.startTime = System.currentTimeMillis();
        // 对战模式（15分钟）鏖战模式（30分钟）
        this.endTime = startTime + (gameType == Room.GameType.ordinary ? 15 * 60 * 1000 : 30 * 60 * 1000);
        this.leftSide = new Side();
        this.rightSide = new Side();
        leftSide.setEnemy(rightSide);
        rightSide.setEnemy(leftSide);
    }

    public String comment(LiveComment comment) {
        if (status != Room.RoomStatus.start) {
            return null;
        }
        Player player = playerMap.get(comment.getSec_openid());
        if (player == null) {
            Player newPlayer = new Player(comment);
            if (comment.getContent().equals("1")) {
                Ship ship = leftSide.addShip(shipIndex++, newPlayer.getOpenId(), Room.ShipType.Level0);
                newPlayer.setBaseShip(ship);
                newPlayer.setSideType(true);
            } else if (comment.getContent().equals("2")) {
                Ship ship = rightSide.addShip(shipIndex++, newPlayer.getOpenId(), Room.ShipType.Level0);
                newPlayer.setBaseShip(ship);
                newPlayer.setSideType(false);
            } else {
                return null;
            }
            playerMap.put(newPlayer.getOpenId(), newPlayer);
            return newPlayer.getOpenId();
        }
        if (comment.getContent().equals("666")) {
            player.getBaseShip().addLike(1);
        }
        return null;
    }

    public void gift(LiveGift gift) {
        if (status != Room.RoomStatus.start) {
            return;
        }
        Player player = playerMap.get(gift.getSec_openid());
        if (player == null) {
            return;
        }
        String giftId = gift.getSec_gift_id();
        MetaGift metaGift = Json.giftIdToMeta.get(giftId);
        Room.ShipType shipType = Room.ShipType.forNumber(metaGift.getType());
        if (shipType == Room.ShipType.Level0) {
            player.getBaseShip().addGift(gift.getGift_num());
        } else {
            for (int i = 0; i < gift.getGift_num(); i++) {
                if (player.isSideType()) {
                    leftSide.addShip(shipIndex++, player.getOpenId(), shipType);
                } else {
                    rightSide.addShip(shipIndex++, player.getOpenId(), shipType);
                }
            }
        }
        giftList.add(new Gift(gift));
    }

    public void like(LiveLike like) {
        if (status != Room.RoomStatus.start) {
            return;
        }
        Player player = playerMap.get(like.getSec_openid());
        if (player == null) {
            return;
        }
        player.getBaseShip().addLike(like.getLike_num());
    }

    public Room.RoomPush round() {
        if (status != Room.RoomStatus.start) {
            return null;
        }
        leftSide.round();
        rightSide.round();
        Map<String, Integer> leftSettle = leftSide.settle();
        for (Map.Entry<String, Integer> entry : leftSettle.entrySet()) {
            Player player = playerMap.get(entry.getKey());
            player.setContribute(player.getContribute() + entry.getValue() / 10.0);
        }
        Map<String, Integer> rightSettle = rightSide.settle();
        for (Map.Entry<String, Integer> entry : rightSettle.entrySet()) {
            Player player = playerMap.get(entry.getKey());
            player.setContribute(player.getContribute() + entry.getValue() / 10.0);
        }
        roundNum++;
        log.info("roomId:{},roundNum:{}", getRoomId(), roundNum);
        int leftPulling = leftSide.getPulling();
        int rightPulling = rightSide.getPulling();
        double pulling = rightPulling - leftPulling;
        if (pulling != 0) {
            //冰山行进速度=（右方托力值 - 左方托力值）/（左方托力值+右方托力值）*标准速度A*（绝对值（右方托力值 - 左方托力值））开n次方 A=5 n=0.1
            speed = pulling / (leftPulling + rightPulling) * 5 * Math.pow(Math.abs(pulling), 0.1);
            icebergPos += speed;
            log.info("roomId:{},icebergPos:{}", getRoomId(), icebergPos);
        } else {
            speed = 0;
        }
        checkEnd();
        return toProto();
    }

    private void checkEnd() {
        if (icebergPos > 1000) {
            status = Room.RoomStatus.rightWin;
        } else if (icebergPos < -1000) {
            status = Room.RoomStatus.leftWin;
        } else if (System.currentTimeMillis() > endTime) {
            if (icebergPos > 0) {
                status = Room.RoomStatus.rightWin;
            } else {
                status = Room.RoomStatus.leftWin;
            }
        }
    }

    private Room.RoomPush toProto() {
        Room.RoomPush.Builder roomPush = Room.RoomPush.newBuilder();
        Room.RoomInfo.Builder room = Room.RoomInfo.newBuilder();
        room.setRoomId(getRoomId());
        room.setGameType(gameType);
        room.setRoomStatus(status);
        room.setIcebergPos(icebergPos);
        room.setStartTime(startTime);
        room.setEndTime(endTime);
        room.setSpeed(speed);
        room.setLeft(leftSide.toProto());
        room.setRight(rightSide.toProto());
        roomPush.setRoom(room);
        roomPush.addAllPlayers(playerMap.values().stream().map(Player::toProto).toList());
        roomPush.addAllGifts(giftList.stream().map(Gift::toProto).toList());
        giftList.clear();
        return roomPush.build();
    }

    public long getRoomId() {
        return anchor.getRoomId();
    }

}
