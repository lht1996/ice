package com.lht.ice.data;

import lombok.Data;

@Data
public class LiveComment {
    private String msg_id;
    private String sec_openid;
    private String content;
    private String avatar_url;
    private String nickname;
    private Long timestamp;
}