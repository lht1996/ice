FROM openjdk:17
LABEL authors="LeHaoTian"
ADD target target
EXPOSE 9360
EXPOSE 9361
#时区
ENV TZ=Asia/Shanghai
ENTRYPOINT ["java", "-jar", "target/tugOfWar-0.0.1-SNAPSHOT.jar"]